import express from 'express'
import { createProductTable } from '../db/db.js'
import productRouter from "./productRouter.js";

createProductTable();

export const server = express();
server.use(express.json());

server.use("/", productRouter);

const PORT = process.env.PORT || 3001
const { PG_HOST, PG_USER, PG_DB, PG_PASSWORD, PG_PORT } = process.env

server.listen(PORT, () => {
    console.log('Version 1');
    console.log('Environment variables:', { PG_HOST, PG_USER, PG_DB, PG_PASSWORD, PG_PORT, PORT })
    console.log(`Listening to port ${PORT}`)
});


