
import express from "express";
import dao from "../db/dao.js";

const router = express.Router();

router.get("/", async (_req, res) => {
    const result = await dao.findAll()
    console.log(result.rows);

    res.send(result);
});

router.post("/", async (req, res) => {
    const product = req.body
    const result = await dao.insertProduct(product);
    const storedProduct = { id: result.rows[0], ...product };
    console.log("post response data", storedProduct);
    res.send(storedProduct);
});

router.put("/:id", async (req, res) => {
    const product = { id: req.params.id, ...req.body }
    const result = await dao.updateProduct(product);
    const updatedProduct = { id: result.rows[0], ...product };
    res.send(updatedProduct);
});

router.delete("/:id", async (req, res) => {
    const id = req.params.id;
    await dao.deleteProduct(id);
    res.status(200).send('Deleted');
});

export default router;