import pg from 'pg';
import queries from './queries.js'

const isProd = process.env.NODE_ENV === 'production';

const { PG_HOST, PG_PORT, PG_USER, PG_PASSWORD, PG_DB } = process.env;

export const pool = new pg.Pool({
    host: PG_HOST,
    port: PG_PORT,
    user: PG_USER,
    password: PG_PASSWORD,
    database: PG_DB,
    ssl: isProd
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result;
    } catch (error) {
        console.error(error.stack);
        error.name = 'dbError';
        throw error
    } finally {
        client.release();
    }
}

export const createProductTable = async () => {
    // await executeQuery('DROP table products');
    await executeQuery(queries.createProductTable)
    console.log('Product table initialization successful')
}

export default { executeQuery, pool }